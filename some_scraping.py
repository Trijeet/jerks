import requests as rq
from bs4 import BeautifulSoup as bs

some_html = """
<html>

<head>
  <style>
    li {font-size: 18px;}
  </style>
  <title>JupyterLab</title>
</head>

<body>
  <div style="border-style: dotted; padding: 10px">
    <h1>Today's Learning Objectives</h1>
    <ul>
      <li>Decipher basic HTML</li>
      <li>Retrieve information from Internet</li>
      <li>Parse web data</li>
      <li>Gather and prepare data systematically</li>
    </ul>
    <br>
  </div>
</body>

</html>
"""

soup = bs(some_html, "lxml")

## Find returns a singular element
soup.find('h1')
## Calling the text attribute of the element returns a string
soup.find('h1').text

## Elements are just objects, so we can save those to variables
my_interesting_element = soup.find('h1')
my_interesting_element.parent

## Find all always returns a list, even if there's only one item.
for item in soup.find_all('li'):
    print(item.text)


def parse_url(url):
    r = rq.get(url)
    if r.status_code==200:
        print('we did it!')
        page = r.text
        return bs(page, "lxml")
    else:
        print('could not get text.')

soup = parse_url("https://www.google.com")

