# Welcome to the getting started page!

## This is to just outline some tools that we'll need to get started working together.

```mermaid
graph TD;
  Bash-->Gitlab;
  Python-->Gitlab;
  Sublime-->Gitlab;
  VSCode-->Gitlab;
  Gitlab-->Zoom;
```


1) Perhaps most importantly, we need to connect! For their suite of tools and for the added value of some anonymity we'll be using a software called Zoom:  :movie_camera:
 - First download the installer: https://zoom.us/download
 - Grab the sign-up link here: https://zoom.us/signup
 - Then verify your e-mail


### We're meeting this Saturday, 01/11/2019 at ```11am EST```
### I'll send out a Zoom invitation the morning of, you'll just have to hit "join" and input the code.
### Also, save yourself some hairs and PIN git bash and sublime text to your taskbar!

<br>

2) Cooperation is at the heart of what we're doing. Working together is a multiplicative effort: twice as many people is truly four times as fast as just the single. We'll be using <i>this</i> site:  :link:
 - Collaboration tool: https://gitlab.com
 - You may have heard of Github. If you have, Gitlab is a less restrictive version of it that doesn't steal your code and information (plus they have a great color scheme).


3. Get git bash  :eyeglasses:
  - Self tracking: https://git-scm.com/downloads
  - Register yourself (copy and paste after changing the quoted parts to match your own)
    - git config --global user.email "user@domain.com"
    - git config --global user.name "First Last"


4. Then get some lightweight development tools:  :pencil2:
 - SublimeText3: https://www.sublimetext.com/3
 - VSCode: https://code.visualstudio.com/download


5. For some basic things we're going to use Python:  :snake:
 - Python3: https://www.python.org/downloads/


## Troubleshooting
<font color="magenta"> I'm dropping a few "gotchas" and short hints that I've put together before here. If you run into any trouble installing these, please let @Trijeet know. </font>

<b> If there are hard disk space/running load problems using these tools, you can omit VSCode</b>
<br>
<b> If there is difficulty setting up ssh or configuring your local Git, please skip this! </b>[^1]

To get started with Gitlab just follow these steps:  
<ol>
<li>Create an account</li>
<li>Verify the account via e-mail</li>
<li>Set-up an ssh connection </li> 
<li>Create your first repository</li>
<li>Push a readme</li>


## Future
<font color="magenta"> Eventually, this page will move to our own dedicated server where we'll all work. I'm still between two choices for what the information hub should be, so stay tuned.</font>
[^1]: If this is unfamiliar to you, please skip this and just make an account! If you wanna give it a shot anyways, visit: https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair