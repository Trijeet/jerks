# Problem Set 1

### String manipulations:

1. Define a function that, given an arbitrary string, will print and return the count the number of vowels in the string to std out.

2. Define a function that, given an arbitrary string, will print and return the count the number of consonants in the string to std out.

3. Define a function that, given two arbitrary strings (A and B), will print and return the number of times string "A" occurs in string "B".

**Hint: remember that you can check for equality using == ("helo"=="hello")**


### Lists all the way down:
Consider the assignment:
x = [["one", "two", "three"], ["six", "seven", "eight", [1, 2, 3]]]
1. Write a routine that switches the values of "one" and "eight" but keeps the rest of the list the same.

2. Write a routine that "corrects" the list: take the list of numbers up one level, so that the final object is one list of 3 lists.

3. Define a function that will print and return the count the number of vowels in the list to std out.

**Hint: functions are meant to be recycled!**