# Problem Set 1 Solutions

### Sets
1. 

2. 

### String manipulations:
1. Define a function that, given an arbitrary string, will print the count the number of vowels in the string to std out.
```python
def vowelize(inputted: str) -> int:
	vowels = 0
	for char in inputted:
		if (char == 'a' or char == 'e' or char == 'i' or char == 'o' or char == 'u'):
			vowels += 1
	print(f"Number of vowels:{vowels}")
	return vowels
```

2. Define a function that, given an arbitrary string, will print the count the number of consonants in the string to std out.
```python
def consonantize(inputted: str) -> int:
	total = len(inputted)
	nconsonants = total - vowelize(inputted)
	print(nconsonants)
	return nconsonants
```

3. Define a function that, given two arbitrary strings, will print and return the number of times string "A" occurs in string "B".
```python
def count_contained_in(input_a: str, input_b: str) -> int:
	count = 0

	for letter in range(len(input_b)):
		temp = input_b[letter:]
		if temp.startswith(input_a):
			count = count + 1
			   
	print(f"Number of times {input_a} occurs in {input_b} is: {count}")
	return count
```