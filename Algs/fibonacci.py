#Define fibonnaci regular version:
def fibonacci(n: int) -> int:
	if n == 0:
		return 0
	if n == 1:
		return 1
	return fibonacci(n-1) + fibonacci(n-2)

x = fibonacci(10)

print(x)

#Examine fibonnaci:
#Runtime is O(2^n)... why?