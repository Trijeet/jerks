#The stack.

class Stack:
	'''Creation of the stack data type'''
	def __init__(self) -> None:
		self.items = []

	def contains(self, to_find: object) -> bool:
		return to_find in self.items

	def peek(self) -> object:
		x = len(self.items)
		if x==0:
			return None
		else:
			return self.items[x-1]

	def pop(self) -> object:
		#Pop removes an item at a specific index
		#Default value of pop is -1, the last item
		return self.items.pop()

	def push(self, item: object) -> None:
		self.items.append(item)



# my_stack = Stack()
# my_stack.push("Wow how cool")
# pv = my_stack.peek()
# print(pv)

# my_stack.push("1")
# my_stack.push(100)
# my_stack.push(650)
# pv = my_stack.contains(650)
# print(pv)
# pv = my_stack.contains("650")
# print(pv)
# pv = my_stack.peek()
# print(pv)