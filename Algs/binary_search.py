#Binary search

def binary_search(sorted_list: list, item: object) -> int:
	'''Binary search takes in a sorted list and returns the index of the element if contained within, and -1 if not found.'''
    first = 0
    last = len(sorted_list) - 1

    while first <= last:
      
        middle = (first + last)//2
       
        if sorted_list[middle] == item:
            return middle
        elif sorted_list[middle] > item:
            last = middle - 1
        else:
            first = middle + 1

    return -1

# testlist = [0, 1, 2, 8, 13, 17, 19, 32, 42,]
# x = binary_search(testlist, 13)
# print(x)

# x = binary_search(testlist, 53)
# print(x)

