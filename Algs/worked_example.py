#The task:

##Suppose that you are hired to design a monitoring system.

##You are to design a system to monitor the 4 members of the team. The team works on a new collection of tasks every week.

##Unfortunately, the team is always very grumpy. No two members actually work together! So, each member needs their own unique workspace.

##Bottom line: create a system that keeps track of the week's list of tasks, what everyone is currently working on, and provide functionality to change each person's list if needed.












##Task Design Plan:
###English


###Abstraction in Design

###Data Structures

###Algorithm to solve